interface DIANDocumentType {
    /**
     * Dian document name
     *
     * @type string
     */
    name: string;
    /**
     * Version of ubl document
     *
     * @type string
     */
    versionId: string;

    /**
     * The dian document profile
     *
     * @type string
     */
    profileId: string;

    /**
     * The document id
     *
     * @type string
     */
    id: string

    /**
     * The number of document
     *
     * @type string
     */
    number: string

    /**
     * The document uuid is the cufe
     *
     * @type string
     */
    uuid?: ElementType | null

    /**
     * The document date
     *
     * @type string
     */
    date: string

    /**
     * The document hour
     *
     * @type string
     */
    hour: string

    /**
     * The concact date and hour
     *
     * @type string
     */
    time: string

    /**
     * The customer of document
     *
     * @type ContactType
     */
    customer: ContactType

    /**
     * The supplier of document
     */
    supplier: ContactType
}

interface ElementType {

}

declare type AddressType =  {
    city?: string,
    county?: string,
    postCode?: string,
    department?: string,
    address: Array<string> | string,
    countryCode?: string
};

declare type IdentificationType = {
    type: number,
    number: number,
    checkDigit?: number
};

declare type PersonType = {
    firstName: string,
    middleName: string,
    lastName: string,
};

interface ContactType {
    organization?: number,
    name: string,
    identification: IdentificationType
    liability: string,
    tax: string,
    address?: AddressType,
    phone?: string,
    email?: string
}

declare type AmountType = {
    currencyId: string,
    value: number
};

declare type TaxSubtotalType = {
    id: number,
    taxable: number,
    rate: number,
    currencyId: string,
    amount?: number
}

declare type TaxTotalType = {
    amount: number,
    isTax?: Boolean,
    currencyId: string,
    subtotal: Array<TaxSubtotalType>
}

declare type SoftwareType = {
    id: string,
    pin: string,
    code?: string
}

declare type ResolutionType = {
    prefix?: string,
    number: string,
    key: string,
    period: {
        start: string,
        end: string
    },
    range: {
        from: string,
        to: string
    }
}

declare type IdType = {
    prefix: string,
    number: number
}


declare type DigestType =  {
    content: string,
    digest: string
}

declare type ContentType = {
    normal: string,
    c14n: string
}

/******************************* AUTH **********************************/

declare type CertType = {
    filename: string,
    password?: string
}

interface AuthType {
    username: string;
    certificate: string | CertType;
}

declare type TransportSearchType = {
    type: string,
    number: string,
    sender: string,
    date: string,
    software: string,
    cufe: string
};

declare type TransportResolutionType = {
    sender: string | number
    provider?: string,
    software?: string
}

declare type AnyType = any;
